var config = require('./config');
var dbPoolOptions = config.dbPoolOptions;
var mysql = require('mysql');

var dbPool = mysql.createPool(dbPoolOptions);

var self = {};
self.query = function(queryStr,args){
    return new Promise(function(resolve,reject){
        var instDb = null;
        getConnection()
            .then(function(connection){
                instDb = connection;
                instDb.query(queryStr,args,function(err,rows,fields){
                    instDb.release();
                    if(err) reject(err);
                    else{
                        var result = {
                            rows:rows,
                            fields:fields
                        };
                        resolve(result);
                    }
                });
            })
            .catch(function(err){
                if(instDb) instDb.release();
                reject(err);
            });
    });
};
self.handleErr = function(err){
    //나중에 DB에 넣읍시다~~!!
    console.log(err);
};

function getConnection(){
    return new Promise(function(resolve,reject){
        dbPool.getConnection(function(err,connection){
            if(err) reject(err);
            else resolve(connection);
        });
    });
}

module.exports = self;